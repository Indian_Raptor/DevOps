#! /bin/sh
echo "Git version"
git version

echo "Docker version"
docker --version

echo "Kubectl version"
kubectl version --client