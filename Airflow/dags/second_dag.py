try:
    from datetime import timedelta
    from airflow import DAG
    from airflow.operators.python_operator import PythonOperator
    from datetime import datetime
    print("All DAG modules are OK..")
except Exception as e:
    print("Error {} ".format(e))


def first_function(*args, **kwargs):
    variable = kwargs.get("name", "Not Found")
    print("Hello World : {}".format(variable))
    return "Hellow world" + variable

with DAG(
        dag_id="second_dag",
        schedule_interval=None,
        default_args={
            'owner': 'airflow',
            'start_date' : datetime(2021, 7, 28),
            'retries': '1',
            'retry_delay': timedelta(minutes=5)
        },
    ) as f:
    
    first_function = PythonOperator(
        task_id="first_function",
        python_callable=first_function,
        op_kwargs={"name":"Arjun Bajpai"}
    )    