try:
    from datetime import timedelta
    from airflow import DAG
    from airflow.operators.python_operator import PythonOperator
    from datetime import datetime
    print("All DAG modules are OK..")
except Exception as e:
    print("Error {} ".format(e))


def first_function(**context):
    print("First Function")
    context['ti'].xcom_push(key='my_key', value="From 1st function")



def second_function(**context):
    instance = context.get("ti").xcom_pull(key='my_key')
    print("From second function : {}".format(instance)) 

with DAG(
        dag_id="third_dag",
        schedule_interval=None,
        default_args={
            'owner': 'airflow',
            'start_date' : datetime(2021, 7, 28),
            'retries': '1',
            'retry_delay': timedelta(minutes=5)
        },
    ) as f:

    first_function = PythonOperator(
        task_id="first_function",
        python_callable=first_function,
        provide_context=True,
        
    )   

    second_function = PythonOperator(
        task_id="second_function",
        python_callable=second_function,
        provide_context=True,
    )   


first_function >> second_function