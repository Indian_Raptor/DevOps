#! /bin/sh

d1="$(date + %s)"
d="$(date +%d.%m.%y)"
bpath="/c/path/DevOps"
n=1

dbckp () {
    start $bpath
}

echo "Creating Backup Folder.."

mkdir $bpath $>/dev/null
ft=$?

mkdir $bpath/bckp-$d-$n &>/dev/null
sts=$?

while [ $sts -gt 0 ]
do
    n=$( expr $n + 1 )
    mkdir $bpath/bckp-$d-$n &> /dev/null
    sts=$?
done

echo "Done.."

if [ $ft -gt 0 ]
then
    dbckp
else
    echo
fi

mv folder1 folder2 $bpath/bckp-$d-$n/

git clone my_repo.git

d2="$(date + %s)"
s=$(($d2 - $d1))
printf 'Total time take by the script to run --> %dm:%ds' $(($s%3600/60)) $(($s%60))

if [ $USER -ne K343060 ]; then
{
echo "Used my command on --> $(date)"
} >> /x/path/$USER.log
fi
