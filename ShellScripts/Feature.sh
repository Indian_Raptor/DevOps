#! /bin/sh

bash /x/path/gcm.sh
echo

for feature in $@
do
    echo "-- Checkingout and Pulling feature/CDW-$feature --"
    echo
    git checkout feature/CDW-$feature
    git pull --all
    git pull --tags
    echo
    echo "-- END --"
    echo
done

if [ $USER -ne K343060 ] ; then
{
echo "Used my command on --> $(date)"
} >> /x/path/$USER.log
fi
