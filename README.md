## This DevOps project demonstrates the ability to take the code from Develop branch to Production (master) via Release branch automatically using two different CI/CD pipelines.

### Step 1: Developer commits and pushes the code to Develop branch which triggers a Jenkins pipeline.


![Step1](/uploads/7d08da2a234839dbe7ba433c40021548/Step1.jpg)


- As soon as the Dveloper commits changes to Develop branch, a Jenkins CI CD pipeline which is hosted as an AWS instance on Ubuntu, is triggered.

### Step 2: The triggered Jenkins pipeline tests the code and if succeeded, push it to Release branch.


![Step2](/uploads/be8c857d21e54446e678e3e43eab27b4/Step2.jpg)


- The Jenkins pipeline tests the code and if passed merges the code to Release branch.

### Step 3: As soon as the Release branch is updated a GitLab pipeline is triggered.


![Step3](/uploads/c19415cb9cbf790bb11248159a6601a4/Step3.jpg)


- As soon as the Release branch is updated, a GitLab CI CD pipeline is triggered, which tests the code and if passed merges the code to master automatically thus completing the cycle.

### Step 4: The GitLab pipeline test the code and pushes it to Production (master) branch.


![Step4](/uploads/6beac418cf5932bdd031f63f6b89287d/Step4.jpg)



#### NOTE: Both Jenkinsfile and .gitlab-ci.yml are present in project repo for reference
