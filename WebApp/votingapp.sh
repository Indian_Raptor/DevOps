#! /bin/sh

kubectl create namespace vote

kubectl create -f k8s-specs/

echo "Access voting app here -> "
minikube service voting-service --url

echo
echo "Access result app here -> "
minikube service result-service --url