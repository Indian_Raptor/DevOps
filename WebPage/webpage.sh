#! /bin/sh

kubectl create namespace mywebpage

kubectl create -f k8s-specs/

echo
var=$(minikube service webpage-service --url)
echo "Access webpage here -> $var/landing.html"