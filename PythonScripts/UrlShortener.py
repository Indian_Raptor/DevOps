import pyshorteners as sh

link = input("Enter URL to Shorten --> ")

s=sh.Shortener(api_key='c1cdfcceb52caf41e996c5227b5850074a7e664d')

print("The shortened url is:")
surl=s.bitly.short(link)
print(surl)

print("Total number of clicks on Shortened url:")
print(s.bitly.total_clicks(surl))
