import gspread
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint

scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/spreadsheets", "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]

credentials = ServiceAccountCredentials.from_jason_keyfile_name('credentials2.json', scope)

client = gspread.authorize(credentials)

sheet = client.open("Portfolio Analysis").sheet3

data = sheet.get_all_records()

print(data)
print(sheet.cell(1,1).value)
print(sheet.row_values(3))
print(sheet.col_values(3))